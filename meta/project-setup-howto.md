# Maquettage
## design
## wireframe
- désigner les zones
- responsive
## UX
- design / wireframe
- impact sur le jeu
- flowchart

# git
- master qui marche
- dev pour tester
- branche par fonctionalité, merge dans dev
- merge dev -> branch1 #pour tester le code au nivau du code (conflicts)
- merge branch1 -> dev #pour tester le code au niveau fonctionnel (algos, variables etc)

# organisation
- architecture des fichiers
- répartition des fonctionnalités
