import * as DOMManager from "./ux/DOMManager.js";

/**
 * changes the sides' border classes to highlight the current player
 * @param {number} sideIndex 
 */
export function highlightPlayerSection(sideIndex) {
    DOMManager.sideElems.forEach(sideElem => {
        sideElem.classList.remove('border-highlight');
    });

    DOMManager.sideElems[sideIndex].classList.add('border-highlight');
}

/**
 * Update the given slot's text based on it's current star value
 * @param {Number} x 
 * @param {Number} y 
 */
export function updateSlotNb (x, y) {
    DOMManager.slotElems[x][y].innerText = data.board[x][y];
}

/**
 * Update all slots' numbers based on their current star value
 */
export function updateAllSlotsNb () {
    for (let x = 0; x < data.board.length; x++) {
        for (let y = 0; y < data.board[x].length; y++){
            updateSlotNb(x, y);
        }
    }
}