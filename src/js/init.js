import * as data from "./data.js";
import * as turnManager from "./turnManager.js";
import * as DOMManager from "./ux/DOMManager.js";
import * as constants from "./constants.js";
import * as visuals from "./visuals.js";
import * as starfieldManager from "./ux/starfieldManager";


document.addEventListener("DOMContentLoaded", initField);

// ===

function initGame() {
    //TODO: move those into data?
    //TODO: initPlayers() or data.init(), redo resetPlayers() to only reset scores
    resetBoard();
    resetPlayers();
    data.setCurrentPlayer(0);

    starfieldManager.resetStars();

    // TODO: use this in play()?
    data.gameStatus.isEnded = false;

    turnManager.startTurn();
}

function initField() {
    DOMManager.init();
    starfieldManager.init();

    var btn = document.getElementById('reset');
    btn.addEventListener("click", initGame);

    // bind play fn on all slots
    for (let x = 0; x < DOMManager.slotElems.length; x++) {
        for (let y = 0; y < DOMManager.slotElems[x].length; y++){
            DOMManager.slotElems[x][y].addEventListener("click", turnManager.play)
        }
    }
}

// ===

function resetBoard() {
    const newBoard = [];
    data.setBoard(newBoard);

    for (let x = 0; x < constants.NB_PLAYERS; x++) {
        const newSide = [];
        newBoard.push(newSide);

        for (let y = 0; y < constants.NB_SLOTS; y++){
            newSide.push(constants.NB_STARS_PER_SLOTS);
            // U use to say what YOU want to push inside, not what you want inside smth
        }
    }
}

function resetPlayers() {
    //TODO: don't add more players if they already exist, just zero out their score
    for (let i = 0; i < constants.NB_PLAYERS; i++) {
        const newPlayer = {
            'score':0
        };

        data.players.push(newPlayer);
    }
}


/*          on insert le lien du bouton au plateau ici



        qui joue = current_player

        choix d 'une case -> cases autorisées

        vider la
        case

        rotation - > poser les graines - > pas dans la
        case de base

        boucle(vérif - > récupérer des graines - > graines++pour current_player) { tant que nb graine est correct }

        vérif - > état de la partie(fin / famine / problème ? )

        changement de joueur *
*/