import * as data from "./data.js"
import * as starfieldManager from "./ux/starfieldManager.js";


export function turn_moveStarvesOpponent() {
    data.setBoard([
        [6, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1]
    ])

    starfieldManager.resetStars();
}
export function turn_moveDoesntStarvesOpponent() {
    data.setBoard([
        [5, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1]
    ])

    starfieldManager.resetStars();
}

export function turn_forcePlayerToFeedOpponent_Top() {
    data.setBoard([
        [1, 2, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0]
    ])

    data.setCurrentPlayer(0);

    starfieldManager.resetStars();
}

export function turn_forcePlayerToFeedOpponent_Bottom() {
    data.setBoard([
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 2, 1]
    ])

    data.setCurrentPlayer(1);

    starfieldManager.resetStars();
}

export function turn_skipStartingSlot() {
    data.setBoard([
        [0, 0, 0, 0, 0, 0],
        [12, 0, 0, 0, 0, 0]
    ])

    starfieldManager.resetStars();
}

export function end_topPlayerGets25Seeds() {
    data.setBoard([
        [1, 0, 0, 0, 0, 0],
        [2, 0, 0, 0, 0, 0]
    ])

    data.players[0].score = 24;

    starfieldManager.resetStars();
}


export function end_topPlayerCantFeedStarvingOpponent() {
    data.setBoard([
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1]
    ]);

    data.setCurrentPlayer(1);
    starfieldManager.resetStars();
}

