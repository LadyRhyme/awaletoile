import * as utils from "../utils.js";
import * as data from "../data.js";
import * as constants from "../constants.js";

export const sideElems = []; // an array of board sides' elems, filled by generateSides()
export const slotElems = []; // a 2d array of each slot within each sides, filled by generateSlots
export const keepElems = [];

// ===
// those should maybe be in an actual init file

const PLAYER_SIDE_CLASSNAME = "player-section";
const SLOT_CLASSNAME = "slot";
const KEEP_CLASSNAME = "keep";

// ===
// these functions generate sides and slots according to defined constants,
// and fill the sideElems and slotElems arrays accordingly.
// ===

function generateSide() {
    const elem = document.createElement("div");

    elem.classList.add(PLAYER_SIDE_CLASSNAME);

    return elem;
}

function generateSides() {
    const boardElem = document.getElementById("board");

    for (let i = 0; i < constants.NB_SIDES; i++) {
        const newSide = generateSide();

        boardElem.appendChild(newSide);
        sideElems.push(newSide);

        //console.log("adding side", i, newSide);
    }
}

function generateSlot(sideIndex, slotIndex) {
    const elem = document.createElement("div");

    elem.classList.add(SLOT_CLASSNAME);
    elem.dataset.sideIndex = sideIndex;
    elem.dataset.slotIndex = slotIndex;

    return elem;
}

function generateSlots() {
    const boardElem = document.getElementById("board");

    sideElems.forEach((sideElem, sideIndex) => {
        for (let slotIndex = 0; slotIndex < constants.NB_SLOTS; slotIndex++) {
            const newSlot = generateSlot(sideIndex, slotIndex);

            if (!slotElems[sideIndex]) {
                slotElems[sideIndex] = [];
            }

            slotElems[sideIndex][slotIndex] = newSlot;
            sideElem.appendChild(newSlot);

            //console.log("adding slot", slotIndex, newSlot);
        }
    })
}

function generateKeep() {
    const elemOuter = document.createElement("div");
    const elemInner = document.createElement("div");

    elemOuter.classList.add(KEEP_CLASSNAME + '_outer');
    elemInner.classList.add(KEEP_CLASSNAME + '_inner');

    elemOuter.appendChild(elemInner);

    return elemOuter;
}

function generateKeeps() {
    sideElems.forEach((sideElem, sideIndex) => {
        const newElem = generateKeep();

        keepElems.push(newElem);
        sideElem.appendChild(newElem);
    })
}

// ===

/**
 * Sets an event listener on all generated slots.
 * @param {string} eventType 
 * @param {function} fn 
 */
export function setEventListenerOnSlots(eventType, fn) {
    const elems = document.getElementsByClassName(SLOT_CLASSNAME);

    // using Array.from() because .getElementsByClassName() returns
    // an array-*like* list, without foreach
    Array.from(elems).forEach(elem => {
        elem.addEventListener(eventType, fn.bind(null, elem, elem.dataset.sideIndex, elem.dataset.slotIndex) );
    })
}

// ===
export function init() {
    generateSides();
    generateSlots();
    generateKeeps();
}
