import * as DOMManager from "./DOMManager.js";
import * as constants from "../constants.js";
import * as utils from "../utils.js";


export const stars = [];

const starsRequiringUpdate = new utils.SetWithCallbacks(null, {
        add: initIntervalIfStarsNeedUpdate,
        delete: clearIntervalIfNoStarToUpdate
});

const updateIntervalMs = constants.STARS_UPDATE_MS;
const STARSHOT_RANGE = 65 / 2; // slot's width|height / 2 - star's width|height

const mousePos = { x: 0, y: 0 };
var updateIntervalId = null;

// ========================

function initIntervalIfStarsNeedUpdate(set) {
    if (set.size > 0 && updateIntervalId == null) {
        //console.log("starting update interval")
        updateIntervalId = setInterval(starsUpdater, updateIntervalMs);
    }
}

function clearIntervalIfNoStarToUpdate(set) {
    if (set.size == 0 && updateIntervalId != null) {
        //console.log("removing update interval")
        clearInterval(updateIntervalId);
        updateIntervalId = null;
    }
}

function starsUpdater() {
    starsRequiringUpdate.forEach(star => star.update());
}

// ========================

function pickRandomStar() {
    return stars[utils.getRandomInt(0, stars.length - 1)];
}

function randomStarShooter() {
    const star = pickRandomStar();

    if (star) {
        star.randomStarShot();
    }

    const nextMove = utils.getRandomInt(19, 30) * 1000;
    setTimeout(randomStarShooter, nextMove);


    //console.log("moved", star, "- next move in", nextMove / 1000, "s")
}

// ========================

class Star {
    constructor(parent) {
        this.elem = document.createElement("img");
        this.elem.classList.add("star");
        this.elem.src = "./media/ETWAL_0" + utils.getRandomInt(1, 5) + ".png";
        this.elem.style.rotate = utils.getRandomInt(0, 360) + "deg";

        parent.appendChild(this.elem);

        // coords are from top right - offset to get the center
        this.x = this.elem.offsetTop - this.elem.clientHeight / 2;
        this.y = this.elem.offsetLeft - this.elem.clientWidth / 2;

        this.originalX = this.x;
        this.originalY = this.y;

        this.movingTo = {x: 0, y: 0};
        this.stepLengths = {x: 0, y: 0};
        this.moveIterations = 0;
        this.moveTotalIterations = 0;

        this.slotPos = {x: 0, y: 0};
    }

    update() {
        //console.log("update()");
        this.moveIterations++;

        this.setX(this.x + (this.stepLengths.x));
        this.setY(this.y + (this.stepLengths.y));

        if (this.moveIterations >= this.moveTotalIterations) {
            //console.log("stopping", this.updateIntevalId);
            starsRequiringUpdate.delete(this);

            this.setX(Math.floor(this.x));
            this.setY(Math.floor(this.y));
        }
    }

    randomStarShot() {
        this.flyTo(
            this.originalX + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE),
            this.originalY + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE),
            utils.getRandomInt(70, 300)
        )
    }

    setX(val) {
        //val = Math.floor(val);

        this.x = val;
        this.elem.style.top = `${val}px`
    }
    setY(val) {
        //val = Math.floor(val);

        this.y = val;
        this.elem.style.left = `${val}px`
    }

    flyToElem(slotElem, slotX, slotY, flightTimeMs = constants.FLIGHT_TIME_MS) {
        const elemXMiddle = slotElem.offsetTop + (slotElem.clientHeight / 2);
        const elemYMiddle = slotElem.offsetLeft + (slotElem.clientWidth / 2);

        this.originalX = elemXMiddle - (this.elem.clientHeight / 2);
        this.originalY = elemYMiddle - (this.elem.clientWidth / 2);

        this.slotPos.x = slotX;
        this.slotPos.y = slotY;

        // land randomly near the slot's center
        this.flyTo(
            elemXMiddle + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE),
            elemYMiddle + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE),
            flightTimeMs
            );
    }

    flyTo(x, y, flightTimeMs = constants.FLIGHT_TIME_MS) {
        this.moveIterations = 0;
        this.moveTotalIterations = flightTimeMs / updateIntervalMs;

        // coords are from top right - offset to get the center
        x -= this.elem.clientHeight / 2;
        x -= this.elem.clientWidth / 2;


        // avoid the mouse cursor
        while (utils.coordsAreWithinRangeOf(x, y, mousePos.x, mousePos.y, 10)) {
            x += utils.getRandomInt(-10, 10);
            y += utils.getRandomInt(-10, 10);
        }

        // get the individual x, y distances
        const distances = {
            x: x - this.x,
            y: y - this.y
        }

        this.movingTo = distances;

        // avoid dividing by 0
        this.stepLengths = {
            x: (distances.x !== 0 ? distances.x / this.moveTotalIterations : 0),
            y: (distances.y !== 0 ? distances.y / this.moveTotalIterations : 0)
        };

        starsRequiringUpdate.add(this);
    }

    teleportTo(x, y) {
        // avoid the mouse cursor
        while (utils.coordsAreWithinRangeOf(x, y, mousePos.x, mousePos.y, 10)) {
            x += utils.getRandomInt(-10, 10);
            y += utils.getRandomInt(-10, 10);
        }

        // coords are from top right - offset to get the center
        this.setX(x - this.elem.clientWidth / 2);
        this.setY(y - this.elem.clientHeight / 2);
    }

    teleportToElem(slotElem, slotX, slotY) {
        const elemXMiddle = slotElem.offsetTop + (slotElem.clientHeight / 2);
        const elemYMiddle = slotElem.offsetLeft + (slotElem.clientWidth / 2);

        this.originalX = elemXMiddle - (this.elem.clientHeight / 2);
        this.originalY = elemYMiddle - (this.elem.clientWidth / 2);

        this.slotPos.x = slotX;
        this.slotPos.y = slotY;

        this.teleportTo(
            elemXMiddle + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE),
            elemYMiddle + utils.getRandomInt(-STARSHOT_RANGE, STARSHOT_RANGE)
        );
    }

    async twinkleIn() {

    }

    async twinkleOut() {

    }
}

// ========================

export function init() {
    console.log("starfield init");
    const starfieldElem = document.getElementById("starfield");

    console.log("generating stars");
    for (let i = 0; i < constants.NB_STARS; i++) {
        stars.push(new Star(starfieldElem));
    }
    console.log("stars", stars);

    //TODO: give stars a random position / opacity?


    document.addEventListener("mousemove", (event) => {
        mousePos.x = event.clientY;
        mousePos.y = event.clientX;
    })

    window.addEventListener("resize", reTeleportStarsToTheirSlots);

    setTimeout(randomStarShooter, utils.getRandomInt(4, 10) * 1000);
}

/**
 * run through the board and move stars to the corresponding slots,
 * then run through the players' scores and set remaining stars to the corresponding keeps
 * then move stars out of the board (for test cases)
 */
export function resetStars() {
    var starIndex = 0;

    // loop through the board and set stars in the corresponding slots
    for (let x = 0; x < data.board.length; x++) {
        for (let y = 0; y < data.board[x].length; y++) {
            for (let starNb = 0; starNb < data.board[x][y]; starNb++) {
                const star = stars[starIndex];

                star.flyToElem(DOMManager.slotElems[x][y], x, y);

                starIndex++;
            }
        }
    }

    // loop through the players and set stars in their score slots
    for (let playerIndex = 0; playerIndex < data.players.length; playerIndex++) {
        const score = data.players[playerIndex].score;

        for (let i = 0; i < score; i++, starIndex++) {
            const star = stars[i + starIndex];
            const targetSlot = DOMManager.keepElems[playerIndex];
            star.flyToElem(targetSlot, null, null);

            star.flyTo(0, 0);
            star.slotPos = {x: 0, y: 0};
        }
    }

    // move remaining stars out of the board
    for (let i = starIndex; i < stars.length; i++) {
        starIndex++;
        const star = stars[i];

        star.flyTo(0, 0);
        star.slotPos = {x: 0, y: 0};
    }
}

/**
 * reposition stars to their slots. To be used when the slots get moved (eg. by resize)
 */
export function reTeleportStarsToTheirSlots() {
    // TODO: fix for starkeeps
    stars.forEach(star => {
        const starsSlotElem = DOMManager.slotElems[star.slotPos.x][star.slotPos.y];
        star.teleportToElem(starsSlotElem, star.slotPos.x, star.slotPos.y);
        star.flyToElem(starsSlotElem, star.slotPos.x, star.slotPos.y);
    });
}

/**
 * Get all stars at a given slot
 * @param {Number} x The slot's X coord
 * @param {Number} y The slot's Y coord
 * @returns {Array} An array of stars at the given slot
 */
export const getStarsAtSlot = (x, y) => stars.filter(star => star.slotPos.x === x && star.slotPos.y === y);

/**
 * Pick a star from the given coordinates' elem and move it to the given coordinates' slot elem
 * @param {Number} fromX x coordinate of the originating slot
 * @param {Number} fromY y coordinate of the originating slot
 * @param {Number} toX x coordinate of the target slot
 * @param {Number} toY y coordinate of the target slot
 */
export function moveStarToSlot(fromX, fromY, toX, toY) {
    const starsAtPos = getStarsAtSlot(fromX, fromY);
    if (starsAtPos.length > 0) {
        const targetSlot = DOMManager.slotElems[toX][toY]
        starsAtPos[0].flyToElem(targetSlot, toX, toY);
    }
}

export function takeStars(fromX, fromY, toKeepIndex) {
    const starsAtPos = getStarsAtSlot(fromX, fromY);
    starsAtPos.forEach(star => {
        const targetSlot = DOMManager.keepElems[toKeepIndex];
        star.flyToElem(targetSlot, null, null);
    })
}
