export const Clockwise = 0
export const CounterClockwise = 1

export const NB_SLOTS = 6;
export const NB_STARS_PER_SLOTS = 4;
export const NB_PLAYERS = 2;
export const NB_SIDES = NB_PLAYERS;
export const NB_STARS = NB_SLOTS * NB_SIDES * NB_STARS_PER_SLOTS;

export const FLIGHT_TIME_MS = 350;
export const STARS_UPDATE_MS = 5;