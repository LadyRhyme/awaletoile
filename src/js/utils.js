/**
 * Returns a promise that resolves after a given time.
 * @param {number} waitTimeMs Time to wait in ms
 * @return {Promise} A promise that resolves after `waitTimeMs` ms.
 */
export const sleep = (waitTimeMs) => new Promise(res => setInterval(res, waitTimeMs));

/**
 * get an integer within min and max
 * @param {Number} min Range's lower bound
 * @param {Number} max Range's upper bound
 * @returns {Number} A random number
 */
export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * 
 * @param {Number} x1 x coord to check
 * @param {Number} y1 y coord to check
 * @param {Number} x2 x coord of the point to check
 * @param {Number} y2 y coord of the point 
 * @param {Number} rangeRadius radius centered around x2 y2 to check for x1 y1's presence in
 */
export function coordsAreWithinRangeOf(x1, y1, x2, y2, rangeRadius) {
    const dx = x2 - x1, dy = y2 - y1;

    return Math.abs(dx) < rangeRadius && Math.abs(dy) < rangeRadius;
}

/**
 * a Set with callbacks on add and delete
 */
export class SetWithCallbacks extends Set {
    constructor(iterable, callbacks = {add: null, delete: null}) {
        super(iterable);

        this.callbacks = callbacks;
    }

    add(value) {
        super.add(value);
        this.callbacks.add && this.callbacks.add(this);
    }

    delete(value) {
        super.delete(value);
        this.callbacks.delete && this.callbacks.delete(this);
    }
}
