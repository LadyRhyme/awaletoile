import * as constants from "./constants.js";
import * as visuals from "./visuals.js";

/**
 * a 2d array of NB_SIDES * NB_SLOTS
 */
export var board = [
//    [4, 4, 4, 4, 4, 4],
//    [4, 4, 4, 4, 4, 4]
];

/**
 * Sets the board with a new variable, since we can't set variables from other modules
 * @param {Array} boardData The new array to set
 */
export function setBoard(boardData) {
    board = boardData;
}

export var players = [
    // {
    //     'score': 0
    // },
    // {
    //     'score': 0
    // }
];

export var currentPlayer = 0;

export function setCurrentPlayer(playerIndex) {
    currentPlayer = playerIndex;

    visuals.highlightPlayerSection(currentPlayer);
}
export function toggleCurrentPlayer() {
    //TODO: use getNextPlayer(currentPlayer)
    const nextPlayer = (currentPlayer + 1) % constants.NB_PLAYERS;
    setCurrentPlayer(nextPlayer);
}

export var gameStatus = {
    'rotation': constants.CounterClockwise,
    'isEnded': false
}
