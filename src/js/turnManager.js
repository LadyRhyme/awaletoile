import * as data from "./data.js";
import * as constants from "./constants.js";
import * as starfieldManager from "./ux/starfieldManager";

// TODO: separate check functions from actual "turn" functions
// TODO: maybe separate "calculation" functions? (calculateDirection, getNextPlayerIndex...)
// TODO: maybe move moveStar, takeStars and takeAllStars in data?

export function startTurn() {
   // TODO: if it's impossible to force a capture, game ends
   if (isOpponentStarving(data.currentPlayer) && !canPlayerFeedOpponent(data.currentPlayer)) {
      //TODO: move this to a takeAllStars(player) function
      for (let x = 0; x < data.board.length; x++) {
         for (let y = 0; y < data.board[x].length; y++) {
            data.players[data.currentPlayer].score += data.board[x][y];
            data.board[x][y] = 0
         }
      }

      // TODO: proper "game over" announcement function
      alert("Player " + (data.currentPlayer + 1) +" won the game !\nHis opponent is starving to death.");
   }
}

const calculateDirection = (sideIndex) => (sideIndex % 2 == 0) ? -1 : 1;
const invertDirection = (direction) => direction * -1;
const getBoardEnd = (direction) => direction === -1 ? 0 : constants.NB_SLOTS - 1;
const getNextPlayerIndex = (currentPlayer) => (currentPlayer + 1) % constants.NB_PLAYERS;

function takeStars(x, y) { //playerSide, slotIndex
   if (data.currentPlayer != x) {
      if (data.board[x][y] === 2 || data.board[x][y] === 3) {
         const direction = calculateDirection(data.currentPlayer);

         data.players[data.currentPlayer].score += data.board[x][y];
         data.board[x][y] = 0;

         starfieldManager.takeStars(x, y, data.currentPlayer);

         // call itself recursively for the next slot
         takeStars(x, y + direction);
      }
   }
}

function isOpponentStarving(x_currentPlayer) {
   const opponentIndex = getNextPlayerIndex(x_currentPlayer);

   return data.board[opponentIndex].every((starsNb) => starsNb == 0);
}

function doesMoveStarvesOpponent(x_sideIndex_end, y_slotIndex_end) {
   //check if we're on the opponent side
   if (x_sideIndex_end !== data.currentPlayer) {
      const direction = calculateDirection(x_sideIndex_end);

      // if the direction is -1, the end slot is at 0 - otherwise it's at the end of the array
      const endPos_y = getBoardEnd();

      // if we're at the end of the player's field
      if (endPos_y === y_slotIndex_end) {
         // if all slots are at 2 or 3 stars taking them will starve the opponent
         return data.board[x_sideIndex_end].every(nbStars => nbStars >= 2 && nbStars <= 3);
      }
   }

   return false;
}

function canPlayerFeedOpponent(x_currentPlayer) {
   for (let y = 0; y < data.board[x_currentPlayer].length; y++) {
      if (doesMoveFeedsOpponent(x_currentPlayer, y)){
         return true
      }
   }
   return false
}

function doesMoveFeedsOpponent(x_currentPlayer, y_slotIndex) {
   const nbStars = data.board[x_currentPlayer][y_slotIndex];
   const direction = calculateDirection(x_currentPlayer);
   const landingSpotY = y_slotIndex + (nbStars * direction);
   const boardEnd = getBoardEnd(direction);

   if (direction === -1) {
      return (landingSpotY < boardEnd);
   } else {
      return (landingSpotY > boardEnd);
   }
}

export function play(event) {
   const slotElem = event.target;

   const x_sideIndex = parseInt(slotElem.dataset.sideIndex);
   const y_slotIndex = parseInt(slotElem.dataset.slotIndex);
   

   if (x_sideIndex == data.currentPlayer) {
      if (data.board[x_sideIndex][y_slotIndex] > 0) {
         if (!isOpponentStarving(x_sideIndex) || doesMoveFeedsOpponent(x_sideIndex, y_slotIndex)) {
            var endSlot = scatterStars(x_sideIndex, y_slotIndex)

            if (!doesMoveStarvesOpponent(endSlot.x, endSlot.y)) {
               takeStars(endSlot.x, endSlot.y);
            }

            TurnEnd();
         }
      }
   }
}

function scatterStars(x, y) {
   //get the number of stars in the slot
   var gatheredStars = data.board[x][y];
   data.board[x][y] = 0;

   var scattererX = x;
   var scattererY = y;
   for (let scatteredStars = 0; scatteredStars < gatheredStars; scatteredStars++) {
      // move y
      const direction = calculateDirection(scattererX);
      scattererY += direction;
      
      // move x (+ reset y)
      if (scattererY + 1 > constants.NB_SLOTS) { //move up
         scattererX = (scattererX - 1) % constants.NB_SIDES;
         scattererY = constants.NB_SLOTS - 1;
      }
      else if (scattererY < 0) { //move down
         scattererX = (scattererX + 1) % constants.NB_SIDES;
         scattererY = 0;
      }

      // Skip the starting slot
      if (x == scattererX && y == scattererY) {
         scatteredStars--
         continue
      }

      // move the stars
      data.board[scattererX][scattererY]++;
      starfieldManager.moveStarToSlot(x, y, scattererX, scattererY);
   }

   // return the last slot we sent a star to
   return {
      "x": scattererX,
      "y": scattererY
   }
}

function TurnEnd() {
   data.toggleCurrentPlayer();
   checkEndScores();

   startTurn();
}

function checkEndScores(){
   for (let i = 0; i < data.players.length; i++) {
      if (data.players[i].score >= 25) {
         //return i

         // TODO: proper "game over" announcement function
         alert("Player "+(i+1)+" won the game !")
      }
   }
}