import './js/init.js'
import "./js/ux/DOMManager.js";
import * as data from "./js/data.js"
import * as starfieldManager from "./js/ux/starfieldManager";

window.starfieldManager = starfieldManager;
window.data = data;

import * as tests from "./js/tests.js"
window.tests = tests;